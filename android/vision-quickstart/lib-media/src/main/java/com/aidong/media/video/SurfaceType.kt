package com.aidong.media.video

/**
 *  author: wangming
 *  email:  cy_wangming@163.com
 *  date:   2020/4/17 4:56 PM
 */
object SurfaceType {
    const val SURFACE_TYPE_NONE = 0
    const val SURFACE_TYPE_SURFACE_VIEW = 1
    const val SURFACE_TYPE_TEXTURE_VIEW = 2
    const val SURFACE_TYPE_SPHERICAL_GL_SURFACE_VIEW = 3
    const val SURFACE_TYPE_VIDEO_DECODER_GL_SURFACE_VIEW = 4
}
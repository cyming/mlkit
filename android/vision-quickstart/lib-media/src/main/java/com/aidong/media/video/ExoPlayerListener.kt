package com.aidong.media.video

/**
 *  author: wangming
 *  email:  cy_wangming@163.com
 *  date:   2020/4/17 4:31 PM
 */
interface ExoPlayerListener {

    fun firstFrame()
}